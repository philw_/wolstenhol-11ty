# Todo

## JavaScript

- Replace Webpack with…
- Parcel? Vite? Rollup?
- Or keep Webpack?

## CSS

- Go through old Vue templates and replace scoped CSS with utilities or custom CSS

## Eleventy

- Move speaking data to a collection?
- Sort out page templates for homepage vs other pages
- Duplicate header data

## Scripts

- Make all music stuff one Alpine component

## Accessibility

- Make invisible scroller items inert
- Card links
- If input intent is keyboard show all Instagram captions
