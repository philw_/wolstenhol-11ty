import 'alpinejs';
import 'wicg-inert';
import 'what-input';
import Horizon from '@mintuz/horizon';
window.Horizon = Horizon;
import lozad from 'lozad';

window.lozad = lozad('.lozad', {
  enableAutoReload: true,
});
window.lozad.observe();

if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('/service-worker.js');
  });
}

window.PwCardMusic = $el => {
  return {
    isPlaying: false,
    progress: 0,
    previewUrl: $el.dataset.previewUrl,
    index: $el.dataset.index,
    init() {
      this.$nextTick(() => {
        if (this.$refs['playButton']) {
          this.$refs['playButton'].setAttribute('role', 'button');
        }
      });
    },
    musicCardButtonPress($dispatch) {
      if (!this.isPlaying) {
        $dispatch('play-preview', {
          src: this.previewUrl,
          index: this.index,
        });
      } else {
        $dispatch('stop-preview', {
          src: this.previewUrl,
          index: this.index,
        });
      }
    },
    playingPreview($event) {
      if ($event.detail.src === this.previewUrl) {
        this.isPlaying = true;
      }
    },
    stoppedPreview($event) {
      if ($event.detail.src === this.previewUrl) {
        this.isPlaying = false;
      }
    },
    previewProgress($event) {
      if ($event.detail.src === this.previewUrl) {
        this.progress = $event.detail.progress;
      }
    },
  };
};

window.PwContact = () => {
  return {
    submitted: false,
    error: null,
    submitForm() {
      const data = new FormData(this.$refs.form);
      fetch('/', {
        method: 'POST',
        body: data,
      })
        .then(response => {
          if (!response.ok) {
            return Promise.reject({
              status: response.status,
              statusText: response.statusText,
              url: response.url,
            });
          }
          return response;
        })
        .then(() => {
          this.submitted = true;
          this.error = null;
        })
        .catch(error => {
          this.error = JSON.stringify(error, null, 2);
          console.error('Contact form error: ', error);
        });
    },
  };
};

window.PwMusic = () => {
  return {
    isPlaying: false,
    src: null,
    init($dispatch) {
      this.$watch('isPlaying', value => {
        $dispatch(value ? 'playing-preview' : 'stopped-preview', {
          src: this.src,
        });
      });
    },
    play(detail) {
      this.isPlaying = false;
      this.src = detail.src;
      this.$el.play();
    },
    playing() {
      this.isPlaying = true;
    },
    pause() {
      this.$el.pause();
      this.isPlaying = false;
    },
    ended() {
      this.isPlaying = false;
      this.src = null;
    },
    timeupdate($event, $dispatch) {
      const progress = Math.round(($event.target.currentTime / $event.target.duration + Number.EPSILON) * 100) / 100;

      if (Number.isNaN(progress)) {
        return;
      } else {
        $dispatch('preview-progress', {
          src: $event.target.src,
          progress: progress,
        });
      }
    },
  };
};

window.PwSimpleScroller = $el => {
  return {
    scrollAmount: null,
    scrollFull: $el.dataset.scrollFull,
    overflowing: {
      left: false,
      right: true,
    },
    init() {
      const firstListItem = this.$el.querySelector('.scroller > li:first-child');
      const lastListItem = this.$el.querySelector('.scroller > li:last-child');

      let observer = new IntersectionObserver(
        (entries, observer) => {
          entries.forEach(entry => {
            const target = entry.target === firstListItem ? 'left' : 'right';
            if (entry.intersectionRatio < 0.95) {
              this.overflowing[target] = true;
            } else {
              this.overflowing[target] = false;
            }
          });
        },
        {
          root: this.$refs.scroller,
          threshold: 0.95,
        }
      );

      observer.observe(firstListItem);
      observer.observe(lastListItem);

      if (this.scrollAmount === null) {
        if (this.scrollFull) {
          this.scrollAmount = this.$refs.scroller.offsetWidth;
        } else {
          this.scrollAmount = this.$refs.scroller.offsetWidth / 2;
        }
      }
    },
    scrollRight() {
      this.$refs.scroller.scrollLeft += this.scrollAmount;
    },
    scrollLeft() {
      this.$refs.scroller.scrollLeft -= this.scrollAmount;
    },
  };
};

window.PwTweets = () => {
  return {
    colcadeInstance: null,
    extraTweets: null,
    colcade() {
      if (window.innerWidth > 767) {
        loadjs('https://cdn.jsdelivr.net/npm/colcade@0.2.0/colcade.js', () => {
          this.colcadeInstance = new Colcade('.tweets-grid', {
            columns: '.tweets-grid__col',
            items: '.tweets-grid__item',
          });
        });
      }
    },
    loadMore() {
      fetch('/partial-tweets.html')
        .then(response => {
          return response.text();
        })
        .then(html => {
          const parser = new DOMParser();
          const doc = parser.parseFromString(html, 'text/html');
          const tweets = doc.getElementById('results').children;

          if (window.innerWidth > 767) {
            this.colcadeInstance.append(tweets);
          } else {
            // Add the new tweets (not using Colcade) as the items only need to be stacked.
            const tweetsFragment = document.createDocumentFragment();
            [...tweets].forEach(tweet => {
              tweetsFragment.appendChild(tweet);
            });
            this.$refs.container.appendChild(tweetsFragment);

            // Move columns to the bottom of the list so that they don't interfere with owl classes.
            const cols = this.$el.querySelectorAll('.tweets-grid__col');
            const colsFragment = document.createDocumentFragment();
            [...cols].forEach(col => {
              colsFragment.appendChild(col);
            });
            this.$refs.container.appendChild(colsFragment);
          }

          window.lozad.observe();
        })
        .catch(err => {
          console.error('Something went wrong.', err);
        });
    },
    twitterIntents() {
      loadjs('https://cdn.jsdelivr.net/gh/BrandwatchLtd/twitter-intents@1.0.0/twitter-intents.min.js', function () {
        const intents = new TwitterIntents();
        intents.register();
      });
    },
    init() {
      const observed = Horizon({
        toObserve: this.$el,
        triggerOnce: true,
        onEntry(entry) {
          entry.target.__x.$data.colcade();
          entry.target.__x.$data.loadMore();
          entry.target.__x.$data.twitterIntents();
        },
      });
    },
  };
};

window.PwHeader = () => {
  return {
    active: null,
    init() {
      let headingsInView = [];
      const io = new IntersectionObserver(
        entries => {
          const exitedElements = entries.filter(entry => !entry.isIntersecting).map(entry => entry.target);
          const enteredElements = entries.filter(entry => entry.isIntersecting).map(entry => entry.target);
          headingsInView = headingsInView.filter(h => !exitedElements.includes(h));
          headingsInView.push(...enteredElements);
          if (headingsInView.length > 0) {
            const navigationApp = document.querySelector('header nav').__x;
            const sectionId = (this.active = headingsInView[0].dataset.section);
            navigationApp.$data.active = sectionId;
          }
        },
        {
          rootMargin: '-120px 0px 0px 0px',
        }
      );
      document.querySelectorAll('h2').forEach(heading => io.observe(heading));
    },
  };
};

window.PwGenre = () => {
  return {
    open: false,
    toggle() {
      this.$el.closest('p').classList.add('select-none');
      setTimeout(() => {
        this.$el.closest('p').classList.remove('select-none');
      }, 1000);
      if (this.open) {
        this.$el.blur();
      }
      this.open = !this.open;
    },
  };
};

document.body.addEventListener(
  'load',
  e => {
    if (e.target.tagName != 'IMG' || !e.target.classList.contains('has-blurry-placeholder')) {
      return;
    }

    // Remove the blurry placeholder.
    e.target.style.backgroundImage = 'none';
  },
  true
);

document.querySelector('html').classList.remove('no-js');
document.querySelector('html').classList.add('js');

new Horizon({
  toObserve: document.querySelector('footer'),
  triggerOnce: true,
  onEntry() {
    loadjs(['https://cdn.jsdelivr.net/npm/speedlify-score@2.0.1/speedlify-score.min.js'], 'speedlify-score');
  },
});

const partyInstagramCards = document.querySelectorAll('.card__instagram--party');
if (partyInstagramCards) {
  partyInstagramCards.forEach(card => {
    new Horizon({
      toObserve: card,
      triggerOnce: true,
      intersectionObserverConfig: {
        rootMargin: '0px 0px -50% 0px',
        threshold: 0.5,
      },
      onEntry(entry) {
        loadjs('https://cdn.jsdelivr.net/npm/canvas-confetti@1.4.0/dist/confetti.browser.min.js', function () {
          // Add a timeout to allow any scrolling to stop.
          window.setTimeout(() => {
            var dimensions = entry.target.getBoundingClientRect();
            var centerXCoord = dimensions.left + window.pageXOffset + dimensions.width / 2;

            var count = 150;
            var defaults = {
              origin: {
                y: 0.5,
                x: centerXCoord / window.innerWidth,
              },
              disableForReducedMotion: true,
            };

            function fire(particleRatio, opts) {
              confetti(
                Object.assign({}, defaults, opts, {
                  particleCount: Math.floor(count * particleRatio),
                })
              );
            }

            fire(0.25, {
              spread: 26,
              startVelocity: 55,
            });

            fire(0.2, {
              spread: 60,
            });

            fire(0.35, {
              spread: 100,
              decay: 0.91,
              scalar: 0.8,
            });

            fire(0.1, {
              spread: 120,
              startVelocity: 25,
              decay: 0.92,
              scalar: 1.2,
            });

            fire(0.1, {
              spread: 120,
              startVelocity: 45,
            });
          }, 700);
        });
      },
    });
  });
}
